#include <ros/ros.h>
#include <ros/console.h>

int main()
{
	if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info) ) {
	   ros::console::notifyLoggerLevelsChanged();
	}

	ROS_INFO("Hello %s", "from ROS!");
	return 0;
}
